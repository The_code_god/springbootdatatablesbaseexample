package com.example.demo.service;

import com.example.demo.model.StudentEntity;
import com.example.demo.repository.StudentEntityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class StudentEntityService extends AbstractService<StudentEntity, StudentEntityRepository> {

    /**
     * Initial data to load into our table
     */
    @PostConstruct
    public void createStudents() {
        save(StudentEntity.builder().name("John").age(18).school("High school of nowhere").build());
        save(StudentEntity.builder().name("Mike").age(19).school("Somewhere federal school").build());
        save(StudentEntity.builder().name("Bill").age(20).school("Middle school of elsewhere").build());
        save(StudentEntity.builder().name("Kyle").age(21).school("South park high school").build());
        save(StudentEntity.builder().name("Jane").age(17).school("High school of nowhere").build());
        save(StudentEntity.builder().name("Kate").age(17).school("Somewhere federal school").build());
        save(StudentEntity.builder().name("Laura").age(17).school("Middle school of elsewhere").build());
        save(StudentEntity.builder().name("Stacy").age(18).school("South park high school").build());
        save(StudentEntity.builder().name("Jake").age(20).school("South park high school").build());
        save(StudentEntity.builder().name("Peter").age(21).school("South park high school").build());
        save(StudentEntity.builder().name("Nick").age(20).school("Middle school of elsewhere").build());
        save(StudentEntity.builder().name("Sol").age(19).school("Junior high school of Spring City").build());
        save(StudentEntity.builder().name("Jamal").age(18).school("Church of St. Patrick").build());
    }
}
