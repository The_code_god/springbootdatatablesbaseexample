package com.example.demo.controller;

import com.example.demo.model.StudentEntity;
import com.example.demo.service.StudentEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class StudentEntityController {

    /**
     * Service to help get data from repository
     */
    @Autowired
    private StudentEntityService studentEntityService;


    /**
     * Endpoint for data tables to get all records from the database
     * @return list of records
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<StudentEntity> getStudents() {
        return studentEntityService.findAll();
    }
}
