package com.example.demo.repository;

import com.example.demo.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Basic jpa repository
 */
@Repository
public interface StudentEntityRepository extends JpaRepository<StudentEntity, Long> {
}

