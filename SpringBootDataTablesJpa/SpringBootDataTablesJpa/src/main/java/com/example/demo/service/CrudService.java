package com.example.demo.service;

import java.util.List;
import java.util.Optional;

/**
 * Simple CRUD interface to work with abstract service (optional) you can use your custom interface if you want
 * @param <T>
 */
public interface CrudService<T> {

    Optional<T> findById(long id);

    List<T> findAll();

    T save(T t);

    List<T> saveAll(List<T> list);

    void delete(long id);

    void delete(T obj);
}
