package com.example.demo.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Simple entity to demonstrate data tables capabilities
 */
@Getter
@Setter
@Entity
public class StudentEntity {

    public StudentEntity() {
    }

    @Builder
    public StudentEntity(String name, Integer age, String school) {
        this.name = name;
        this.age = age;
        this.school = school;
    }

    /**
     * Sequence generator starts from id = 100000, you can use any generation strategy you want
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_generator")
    @SequenceGenerator(name = "student_generator", sequenceName = "student_seq", initialValue = 100000)
    private Long studentId = 0L;

    private String name;
    private Integer age;
    private String school;
}
