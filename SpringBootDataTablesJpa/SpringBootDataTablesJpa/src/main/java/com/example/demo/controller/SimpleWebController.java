package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller to output our simple html page with simple script
 */
@Controller
@RequestMapping("/")
public class SimpleWebController {

    /**
     * Map our page to root path of the servlet
     *
     * @return
     */
    @GetMapping("")
    public String student() {
        return "html/index.html";
    }
}
